const mongoose = require('mongoose')
const validators = require('.validators.js')
var Schema = mongoose.Schema

module.exports = {
  name: 'Titulation',
  definition: {
    enum: Number,
    description: String
  },
  options: {
    strict: false,
    collection: 'titulation'
  }
}