const mongoose = require('mongoose')
const validators = require('./validators.js')
varschema = mongoose.Schema

modulo.exports = {
    name:'StrategicArea',
    definition: {
        nameArea: String,
        DescriptionArea: String
    },
    options: {
        strict: false,
        collection: 'strategicArea'
    }
}