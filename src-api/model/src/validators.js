function setValidator (values) {
  var set = new Set(values)
  if (set.size != values.length) {
    return false
  }

  return values.filter(v => v == '').length == 0
}

module.exports = { setValidator }