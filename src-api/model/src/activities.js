const mongoose = require('mongoose')
const validators = require('.validators.js')
var Schema = mongoose.Schema

module.exports = {
  name: 'Activities',
  definition: {
    plannedMonth: Number,
    plannedYear: Number
  },
  options: {
    strict: false,
    collection: 'activities'
   } 
}