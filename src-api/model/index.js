const config = require('../config.js').getApiConfig()
const mongoose = require('mongoose')
const db = mongoose.createConnection(config.mongoDB_URL, { useNewUrlParser: true })
const modelSources = [
  require('./src/project.js'),
  require('./src/activities.js'),
  require('./src/activity.js')
]

console.log('** Model Initialization **')
console.log('Current stage:', config.current_stage)

var models = {}
modelSources.forEach(m => {
  m.options.timestamps = true
  var schema = new mongoose.Schema(m.definition, m.options)

  if (m.indexes != null) {
    m.indexes.forEach(idx => {
      schema.index(idx.fields, idx.options)
    })
  }

  var model = db.model(m.name, schema)

  console.log('Mapping entity:', m.name)
  models[m.name] = model
})

function get(modelName) {
  return models[modelName]
}

function getConnection() {
  return db
}

module.exports = { get, getConnection }
