const model = require('../model')
const Project = model.get('Project')

function createProject(projectData) {
  var project = new Project(projectData)
  return project.save()
}

module.exports = { createProject }